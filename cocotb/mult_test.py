#!/usr/bin/python3
import cocotb
from cocotb.triggers import Timer
from cocotb.triggers import FallingEdge
from cocotb.triggers import RisingEdge
from cocotb.clock import Clock
from cocotb.queue import Queue

clock_coro = None
async def start_clock(dut, period):
    global clock_coro
    clock_coro = await cocotb.start(Clock(dut.clock, period, units="ns").start())

def clear_inputs(dut):
    dut.reset.value = 0
    dut.mcand.value = 0
    dut.mplier.value = 0
    dut.start.value = 0

async def start_mult(dut, a, b):
    dut.mcand.value = a
    dut.mplier.value = b
    dut.start.value = 1
    await FallingEdge(dut.clock)
    clear_inputs(dut)

async def drain_mult(dut):
    # drain multiplier
    for _ in range(8):
        await FallingEdge(dut.clock)

@cocotb.test()
async def starter_test(dut):
    
    await start_clock(dut, 12)

    clear_inputs(dut)
    dut.reset.value = 1
    await FallingEdge(dut.clock)
    await FallingEdge(dut.clock)
    dut.reset.value = 0

    await start_mult(dut, 15, 21)
    product = 15 * 21

    await RisingEdge(dut.done)
    await FallingEdge(dut.clock)
    assert dut.product.value.integer == product, "Multiplier returned %d * %d = %d. Expected %d" % (15, 0x15, dut.product.value.integer, product)

    await drain_mult(dut)

class MultiplierMonitor:

    def __init__(self, dut, num_stages):
        self.num_stages = num_stages
        self.qu = Queue[int](num_stages)
        self.dut = dut
        self.pass_count = 0

    async def monitor_in(self):
        ''' Monitors multiplier signals '''
        start = self.dut.start
        a = self.dut.mcand
        b = self.dut.mplier
        clock = self.dut.clock

        while True:
            await FallingEdge(clock)
            
            if start.value == 1:
                expected_prod = a.value.integer * b.value.integer
                self.qu.put_nowait(expected_prod)
                # print("Started: ", expected_prod)

    async def monitor_out(self):
        ''' Monitors multiplier signals '''
        reset = self.dut.reset
        clock = self.dut.clock
        prod = self.dut.product
        done = self.dut.done

        while True:
            await FallingEdge(clock)
            if reset.value == 1:
                self.qu = Queue[int](self.num_stages)

            if done.value == 1:
                expected = self.qu.get_nowait()
                assert prod.value.integer == expected
                self.pass_count += 1
                # print("Finished: ", expected)

    def run(self):
        ''' Monitors multiplier inputs '''
        cocotb.start_soon(self.monitor_in())
        cocotb.start_soon(self.monitor_out())
           
@cocotb.test()
async def automatic_test1(dut):
    ''' Test Multiplier with a monitor coroutine '''

    await start_clock(dut, 12)


    monitor = MultiplierMonitor(dut, 8)

    clear_inputs(dut)
    dut.reset.value = 1
    await FallingEdge(dut.clock)
    await FallingEdge(dut.clock)
    dut.reset.value = 0

    monitor.run()
    mult_count = 0

    for i in range(0, 10):
        for j in range(0, 10):
            await start_mult(dut, i, j)
            mult_count += 1
        
    await drain_mult(dut)

    # report results
    print("Summary: Performed %d/%d multiplications" % (monitor.pass_count, mult_count))
    assert monitor.pass_count == mult_count

@cocotb.test()
async def automatic_test2(dut):
    ''' Test Multiplier squaring numbers '''

    await start_clock(dut, 12)

    monitor = MultiplierMonitor(dut, 8)

    clear_inputs(dut)
    dut.reset.value = 1
    await FallingEdge(dut.clock)
    await FallingEdge(dut.clock)
    dut.reset.value = 0

    monitor.run()
    mult_count = 0

    for i in range(0, 25):
        await start_mult(dut, i, i)
        mult_count += 1
        
    await drain_mult(dut)

    # report results
    print("Summary: Performed %d/%d multiplications" % (monitor.pass_count, mult_count))
    assert monitor.pass_count == mult_count
