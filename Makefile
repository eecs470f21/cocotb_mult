# To compile additional files, add them to the TESTBENCH or SIMFILES as needed
# Every .vg file will need its own rule and one or more synthesis scripts
# The information contained here (in the rules for those vg files) will be
# similar to the information in those scripts but that seems hard to avoid.
#

# added "SW_VCS=2011.03 and "-full64" option -- awdeorio fall 2011
# added "-sverilog" and "SW_VCS=2012.09" option,
#	and removed deprecated Virsim references -- jbbeau fall 2013
# updated library path name -- jbbeau fall 2013

# VCS = SW_VCS=2017.12-SP2-1 vcs +v2k -sverilog +vc -Mupdate -line -full64

export SW_VCS=2017.12-SP2-1
LIB = /afs/umich.edu/class/eecs470/lib/verilog/lec25dscc25.v
VCS_ARGS=+v2k -sverilog +vc -Mupdate -line -full64

### EDIT BELOW THIS LINE VVVVVVV
TBDIR = $(PWD)/cocotb
TESTBENCH = mult_test
SIMFILES = $(PWD)/pipe_mult.sv $(PWD)/mult_stage.sv
SYNFILES = $(PWD)/mult.vg 
TOP = mult
VCS_ARGS +=
### EDIT ABOVE THIS LINE ^^^^^^^

all: sim

mult.vg:	pipe_mult.sv mult_stage.vg mult.tcl
	dc_shell-t -f ./mult.tcl | tee mult_synth.out

mult_stage.vg:	mult_stage.sv mult_stage.tcl
	dc_shell-t -f ./mult_stage.tcl | tee mult_stage_synth.out

synthesis: mult_stage.vg mult.vg 

sim:
	cd $(TBDIR) && \
		VERILOG_SOURCES="$(SIMFILES)" \
		TOPLEVEL="$(TOP)" \
		MODULE="$(TESTBENCH)" \
		COMPILE_ARGS="$(VCS_ARGS)" \
		SIM_BUILD="$@_build" \
		$(MAKE) -f cocotb.mk
		
.PHONY: sim

dve:
	cd $(TBDIR) && \
		GUI="1" \
		VCS_ARGS+="+memcbk -R -gui" \
		VERILOG_SOURCES="$(SIMFILES)" \
		TOPLEVEL="$(TOP)" \
		MODULE="$(TESTBENCH)" \
		COMPILE_ARGS="$(VCS_ARGS)" \
		SIM_BUILD="$@_build" \
		$(MAKE) -f cocotb.mk

syn: synthesis
	cd $(TBDIR) && \
		VERILOG_SOURCES="$(LIB) $(SYNFILES)" \
		TOPLEVEL="$(TOP)" \
		MODULE="$(TESTBENCH)" \
		COMPILE_ARGS="$(VCS_ARGS)" \
		SIM_BUILD="$@_build" \
		$(MAKE) -f cocotb.mk

syn_dve: synthesis
	cd $(TBDIR) && \
		GUI="1" \
		VCS_ARGS+="+memcbk -R -gui" \
		VERILOG_SOURCES="$(LIB) $(SYNFILES)" \
		TOPLEVEL="$(TOP)" \
		MODULE="$(TESTBENCH)" \
		COMPILE_ARGS="$(VCS_ARGS)" \
		SIM_BUILD="$@_build" \
		$(MAKE) -f cocotb.mk

clean:
	rm -rvf command.log defaults.svf results.xml
	cd $(TBDIR) && rm -rvf *_build *.vpd results.xml ucli.key
	
nuke:	clean
	rm -rvf *.vg *.rep *.db *.chk *.log *.out *.ddc *.svf
	cd $(TBDIR) && rm -rvf DVEfiles/ __pycache__
	
.PHONY: dve clean nuke	
